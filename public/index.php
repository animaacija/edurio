<?php declare(strict_types=1);

try {
    $db = new PDO('mysql:dbname=foo;host=mysql57', 'root', 'pass');
} catch (PDOException $e) {
    die("DB not ready yet... {$e->getMessage()}");
}

$path = $_SERVER['PATH_INFO'] ?? '/';

if ($path === '/dbs/foo/tables/source/json') {
    header('Content-Type: application/json');

    $page = (int) $_REQUEST['page'] ?? 0;
    $page_size = (int) $_REQUEST['page_size'] ?? 0;

    if (!$page || !$page_size) {
        die('incorrect parameters');
    }

    $offset = ($page - 1) * $page_size;
    $stmt = $db->query("SELECT * FROM source LIMIT {$page_size} OFFSET {$offset}", PDO::FETCH_ASSOC);
    echo json_encode($stmt->fetchAll());
} else if ($path === '/dbs/foo/tables/source/csv') {
    header('Content-Encoding: chunked');
    header('Transfer-Encoding: chunked');
    header('Content-Type: text');
    header('Connection: keep-alive');

    ob_start();
    $row = 'a,b,c' . PHP_EOL;
    printf("%x\r\n%s\r\n", strlen($row), $row);

    $stmt = $db->query("SELECT * FROM source", PDO::FETCH_NUM);
    while ([$a, $b, $c] = $stmt->fetch()) {
        $row = "{$a},{$b},{$c}\n";
        printf("%x\r\n%s\r\n", strlen($row), $row);

        if (ob_get_length() > 40) {
            ob_flush();
        }
    }

    ob_end_flush();
} else {
    die('unknown API method');
}
