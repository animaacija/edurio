#!/bin/bash
_PWD=$(cd $(dirname $0) && pwd)
NS=talbergs

[ -z "$(docker network ls -q --filter "Name=$NS")" ] &&
    echo Network created $(docker network create $NS)

[ -z "$(docker image ls --format "{{.ID}}" --filter "label=$NS=php")" ] && \
    echo Building image "$NS=php" && \
    (docker build -t $NS:php --label $NS=php --file $_PWD/Dockerfile.php-74 $_PWD)

[ -z "$(docker image ls --format "{{.ID}}" --filter "label=$NS=mysql")" ] && \
    echo Building image "$NS=mysql" && \
    (docker build -t $NS:mysql --label $NS=mysql --file $_PWD/Dockerfile.mysql-57 $_PWD)

docker run -d --rm --network $NS --env MYSQL_ROOT_PASSWORD=pass -v $_PWD/seed.sql:/docker-entrypoint-initdb.d/seed.sql --name mysql57 \
    $(docker image ls --format "{{.ID}}" --filter "label=$NS=mysql")

docker run -d --rm --network $NS -p 8080:8080 -v $_PWD:/app --workdir /app \
    $(docker image ls --format "{{.ID}}" --filter "label=$NS=php") \
    php -S 0.0.0.0:8080 -t /app/public

echo serving at localhost:8080, refresh once you see that database is ready
