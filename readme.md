To spin up services on localhost:8080 use this script:
```bash
~/path/to/serve.sh # modify if different host port is needed
```
